function Point (x, y) {
	this.x = x;
	this.y = y;
}

Point.prototype.toString = function() {
	return '('+this.x+', '+this.y+')';
}

var point = new Point(17,4);
console.log(point.toString());