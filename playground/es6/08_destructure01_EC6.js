let companyObject = { name: 'Adesso', place: 'Zürich' };

let { name: companyName, place: companyPlace } = companyObject;
console.log(companyName + ' (' + companyPlace + ')');

// let { name, place } = companyObject;
// console.log(name + ' (' + place + ')');

// let { name, place, business='Tech' } = companyObject;
// console.log(name + ' (' + place + ') = ' + business);
