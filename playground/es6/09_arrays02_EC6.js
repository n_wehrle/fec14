// destructuring
let [x, y, ...rest] = [ 'a', 'b', 'c', 'd' ];
console.log(x + ', ' + y + ', [' + rest + ']');

// examples
let [all, year, month, day] = /^(\d\d\d\d)-(\d\d)-(\d\d)$/ .exec('2014-09-22');
console.log(year);

// new methods
console.log([5, 9, 12].find(x => x % 2 === 1 && x > 5));
console.log([5, 9, 12].findIndex(x => x % 2 === 1 && x > 5));