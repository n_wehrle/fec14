function Person() {
	this.age = 0;

	// lexically bind 'this'
	setInterval(() => {
		this.age++;
	}, 1000);
}

var p = new Person();
setInterval(function () {
	console.log(p.age)
}, 300);
