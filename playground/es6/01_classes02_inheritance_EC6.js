class Point {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
	toString() {
		return '('+this.x+', '+this.y+')';
	}
}

/* ======================================== */

class ColorPoint extends Point {
	constructor(x, y, color) {
		super(x, y);
		this.color = color;
	}
	toString() {
		return this.color+' '+super();
	}
}

let point = new ColorPoint(17, 4, "red");
console.log(point.toString());