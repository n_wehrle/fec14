/*
  ECMA5: jQuery, when, rsvp
  ECMA6: compatibility instead of different libs
         performance (no extra-lib)
         Browser-API may return promises
         standardization = best practice instead of custom hacks
*/

function fail(){
	throw new Error('Epic fail!');
}

var promise = new Promise(function(resolve, reject){
	setTimeout(function() {
		resolve(42);
	}, 2000);
});

promise
  .then(fail)
  .then(function(result){
  	console.log('Epic win! - ' + result);
  }, function(err){
  	console.log(err.toString());
  });

console.log('SYNC CODE');