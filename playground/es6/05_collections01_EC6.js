let coworkers = new Set();

coworkers.add('Patrice');
coworkers.add('Emre');
coworkers.add('Emre');
coworkers.add('Emre');

console.log(coworkers.has('Norman'));
console.log(coworkers.has('Emre'));

console.log(coworkers);

/*
for (var coworker of coworkers) {
	console.log('Name: ' + coworker);
}
*/