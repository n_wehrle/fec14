// Multiple return values

function ex03() {
	return {forename: "Norman", surename: "Wehrle"};
}

var person = ex03();
console.log(person.forename);