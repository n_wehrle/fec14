// Browser:
// Revealing module pattern: IIFE (immediately invoked function expression)
//
// AMD Modules, CommonJS

// === Declaration ========================================================
var Module = (function() {
    var privateFunc = function() {
        // ...
    };

    var publicFunc1 = function() {
        // ...
    };

    var publicFunc2 = function() {
        // ...
    };


    return {
        publicFunc1: publicFunc1,
        publicFunc2: publicFunc2
    };
}());

// === Usage ==============================================================
var result = Module.publicFunc1();