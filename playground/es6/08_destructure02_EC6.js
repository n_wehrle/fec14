// Multiple return values

function ex03() {
	return {forename: "Norman", surename: "Wehrle"};
}

let {surname, forename} = ex03();
console.log(forename);