function Person() {
	var that = this;
	that.age = 0;

	setInterval(function growUp() {
		that.age++;
	}, 1000);
}

var p = new Person();
setInterval(function () {
	console.log(p.age)
}, 300);
