# Frontendconf 2014 Retrospective

Me and a collegue attended the Frontendconf 2014 in Zurich (Switzerland). This is the talk that summarizes all the shiny new stuff frontenddevelopers can play with in 2014 and 2015. The talk has been held on 22 September 2014 by Norman Wehrle and Patrice Mueller in the adesso AG Switzerland.

![Coverpic](https://bitbucket.org/n_wehrle/fec14/raw/master/coverpic.jpg)

## Topics
- EcmaScript 6 (45min)
- CSS Grid Design (35min)
- Gossip and tools (20min)

## CSS-based SlideShow System
Warning: Only works in latest Firefox, Opera, Safari or Chrome.
For more information, see the [sample slideshow](http://lea.verou.me/csss/sample-slideshow.html)